/*
Chrome Extension for Movie Notes (motes.cselian.com)
Author: Imran Ali Namazi <imran@cselian.com>
Date: May 2015
*/

var motesTab;

function readImdbTab() {
	var queryInfo = { active: true, currentWindow: true };

	chrome.tabs.query(queryInfo, function(tabs) {
		var tab = tabs[0];
		var url = tab.url;

		$('#no-imdb').hide();
		$('#no-motes').hide();

		if (url.indexOf('imdb.com', 0) == -1)
		{
			$('#no-imdb').show();
			$('.motesContainer').hide();
			return;
		}

		queryInfo = { active: false, currentWindow: true };

		motesTab = null;
		chrome.tabs.query(queryInfo, function(tabs) {
			for(i = 0; i < tabs.length; i++)
			{
				if (tabs[i].url == 'http://motes.cselian.com/explore' 
					|| tabs[i].url == 'http://localhost/cs/subs/yii/motes/explore')
				{
					motesTab = tabs[i];
					chrome.tabs.executeScript(null, {file: "readImdbPage.js"});
					break;
				}
			}
			if (motesTab == null)
			{
				$('#no-motes').show();
				$('.motesContainer').hide();
			}
		});
	});
}

readImdbTab();
