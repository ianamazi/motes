<?php
$this->breadcrumbs=array(
	'Explore',
);?>

<form method="post">
<?php
if (isset($items))
{
	echo '<h1>Add Notes</h1>';
	echo '<ul class="imdblinks">';
	$i = 0;
	foreach ($items as $itm)
	{
		$i++;
		echo '<li>';
		if ($itm['type'] == 'unknown')
		{
			echo 'Unknown url: ' . $itm['url'];
			echo '</li>' . PHP_EOL;
			continue;
		}

		echo '<input type="text" name="id"' . $i . ' value="' . $itm['id'] . '" /> ';
		if ($itm['type'] == 'movie')
		{
			echo '<input type="radio" name="seen' . $i . '" value="set" id="seen' . $i . '" />';
			echo '<label for="seen' . $i . '">seen</label>';
			echo '<input type="radio" name="watch' . $i . '" value="set" id="seen' . $i . '" />';
			echo '<label for="watch' . $i . '">watch</label>';
			echo '<input type="text" name="prio' . $i . '" value="5" />';
		}
		elseif ($itm['type'] == 'person')
		{
			echo '<input type="radio" name="like' . $i . '" value="set" id="like' . $i . '" />';
			echo '<label for="like' . $i . '">like</label>';
			echo '<input type="radio" name="dont' . $i . '" value="set" id="dont' . $i . '" />';
			echo '<label for="dont' . $i . '">dont</label>';
			echo '<input type="text" class="spacer" />';
		}
		echo ' | <input type="text" name="note' . $i . '" value="" class="note" />';

		echo '</li>' . PHP_EOL;
	}
	echo '</ul>';
	echo '<input type="submit" name="submitnotes" value="Save Movie and Actor Notes"><br/><br/>';
}

if (isset($message))
{
	echo '<div class="message">' . $message . '</div>';
}
else
{
?>

<h1>Explore Imdb</h1>
<p>Paste urls from imdb here or use the <a href="<?php echo Yii::app()->baseUrl; ?>/downloads/chrome.crx">Chrome Plugin</a>. Until its available in the chrome store, you can go to chrome://extensions and add this <a href="<?php echo Yii::app()->baseUrl; ?>/downloads/motes.zip">zip file</a>. Please note that you need to check developer mode and then say load unpacked extension.</p>
	<textarea name="imdblinks" style="width: 1250px; position: relative; left: -150px" rows="10"
>http://www.imdb.com/name/nm0000199/	Al Pacino	M/MV5BMTQzMzg1ODAyNl5BMl5BanBnXkFtZTYwMjAxODQ1._V1_SX214_CR0,0,214,317_AL_.jpg
http://www.imdb.com/title/tt0086250/	Scarface	M/MV5BMjAzOTM4MzEwNl5BMl5BanBnXkFtZTgwMzU1OTc1MDE@._V1_SX214_AL_.jpg
http://www.imdb.com/name/nm0000661/	Donald Sutherland	M/MV5BMTc0MDI1NzcyMl5BMl5BanBnXkFtZTcwOTk0MjQwOQ@@._V1_SY317_CR15,0,214,317_AL_.jpg
http://www.imdb.com/title/tt0070666/	Serpico	M/MV5BMTkyNzQ2MjkzNl5BMl5BanBnXkFtZTgwODM2MzcxMTE@._V1_SX214_AL_.jpg
http://www.imdb.com/list/ls006643593		
</textarea><br /><br />
	<input type="submit" name="submiturls" value="Read Movies and Actors">
<?php
}
?>
</form>
