<?php $this->pageTitle=Yii::app()->name; ?>

<h1>Welcome</h1>

<p>This website lists all movies and actors that have been added by its users.</p>

<p>For movies, it lets you track whether you have seen them or not, whether its 
on your watch list and at what priority as well as some notes about the movie.</p>

<p>For actors (and actresses) you can choose whether they are known to you or not.
In the future, we hope to have fun features list sorting all known (to you) actors by age,
number of movies etc.</p>

<p>Click the explore link to visit imdb.com with our assisted browsing from which 
its easy to see your motes of information about the movie or actor that you are viewing</p>

<p>This site is still in 
<a href="http://en.wikipedia.org/wiki/Software_release_life_cycle#Beta" target="_blank">beta</a>,
so you are encouraged to see the 
<a href="https://bitbucket.org/ianamazi/motes/wiki/Home" target="_blank">wiki</a> or log 
<a href="https://bitbucket.org/ianamazi/motes/issues" target="_blank">issues</a>.
Please make sure to look for similar issues before creating new ones</p>

