<?php
$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<h1>About</h1>

<p>Motes is a fun-to-develop app built as a service to all movie fans and buffs 
and also as a challenge to its designer Imran and a platform for him to showcase 
his <a href="http://www.yiiframework.com/" target="_blank">Yii</a>,
<a href="https://jquery.com/" target="_blank">JQuery</a>, and
<a href="http://en.wikipedia.org/wiki/Web_scraping" target="_blank">Web Scraping</a>.</p>

<p>Its open source so collaborators are welcome and the sourcecode can be seen 
<a href="https://bitbucket.org/ianamazi/motes/">here</a>.</p>

<h1>History</h1>
<ul>
<li>May 2015 - Started work</li>
</ul>
