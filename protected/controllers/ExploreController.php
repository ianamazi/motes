<?php

class ExploreController extends Controller
{
	public function actionIndex()
	{
		$data = array();

		if (isset($_POST['submitnotes']))
		{
			$data['message'] = 'Notes will be saved later.';
		}
		else if (isset($_POST['submiturls']))
		{
			$lines = explode(PHP_EOL, $_POST['imdblinks']);
			$items = array();
			foreach ($lines as $line)
			{
				if (trim($line) == '') continue;
				$bits = explode('	', $line);
				$link = $bits[0];
				if (substr($link, -1) === '/') $link = substr($link, 0, strlen($link) - 1);
				$posName = stripos($link, '/name/');
				$posMovie = stripos($link, '/title/');
				$item = array();
				if ($posName !== false)
				{
					$item['type'] = 'person';
					$item['id'] = substr($link, $posName + strlen('/name/'));
				}
				else if ($posMovie !== false)
				{
					$item['type'] = 'movie';
					$item['id'] = substr($link, $posMovie + strlen('/title/'));
				}
				else
				{
					$item['type'] = 'unknown';
					$item['url'] = substr($link, strlen('http://www.imdb.com/'));
				}
				$items[] = $item;
			}
			$data['items'] = $items;
		}
		
		$this->render('index', $data);
	}
}
