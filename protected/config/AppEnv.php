<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/motes
 * @copyright Copyright &copy; 2013-2015 cselian.com
 * @license http://tg.cselian.com/licenses/motes
 * Manages the variations by domains (environments) for a Multisite Install
 */

class AppEnv
{
	private static $initialized = false;
	
	private static function init($dom)
	{
		self::$initialized = true;

		self::$data[$dom]['smtp'] = array('host' => 'smtp.gmail.com', 'username' => 'intermart@cselian.com', 'password' => 'int3rm@rty123', 'port' => 465, 'encryption' => 'ssl');

		if ($dom == 'localhost') self::$data[$dom]['gii'] = 1;
	}

	private static $data = array(
		'localhost' => array(
			'name' => 'Motes :: Movie Notes Dev'
			, 'code' => 'dev'
			, 'testEmail' => 'imran@cselian.com'
			, 'db' => array('connectionString' => 'mysql:host=localhost;dbname=motes', 'username' => 'root', 'password' => '')
			, 'nonProduction'=> 1
		),
		'motes.cselian.com' => array(
			'name' => 'Motes :: Movie Notes'
			, 'code' => 'live'
			, 'db' => array('connectionString' => 'mysql:host=localhost;dbname=cselian_motes', 'username' => 'nongit', 'password' => 'nongit')
		),
	);
	
	// Helper function for Array
	public static function valOrDefault($array, $name, $default)
	{
		return isset($array[$name]) ? $array[$name] : $default;
	}

	public static function getArrayVal($var, $key, $name)
	{
		$arr = self::get($var, $name);
		return $arr ? $arr[$key] : false;
	}

	public static function get($var, $name, $default = null)
	{
		return self::getDomainVar($var, $name, $default);
	}

	public static function db()
	{
		return self::getDomainVar('db', 'Connection String');
	}
	
	public static function name()
	{
		return self::getDomainVar('name', 'Site Name', 'Motes :: Movie Notes');
	}
	
	private static function getDomainVar($var, $friendly, $default = null)
	{
		$dom = $_SERVER['HTTP_HOST'];

		if (!isset(self::$data[$dom]))
		{
			if ($default !== null) return $default;
			throw new Exception(sprintf('Domain "%s" not configured!', $dom));
		}
		
		if (!self::$initialized) self::init($dom);
		$domData = self::$data[$dom];
		if (!isset($domData[$var]))
		{
			if ($default !== null) return $default;
			throw new Exception(sprintf('%s not configured for domain "%s"!', $friendly, $dom));
		}
		
		return $domData[$var];
	}
	
	private static function contains($haystack, $needle) { return gettype(strpos($haystack, $needle)) == "integer"; }
}
